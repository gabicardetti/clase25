import ChatRepository from "../repositories/chatRepositoryMongo.js"

const repository = new ChatRepository();

export const saveNewMessage = (msg) => {
    repository.save(msg);
}

export const getAllMessagesFromFile = async () => {
    const chatHistory = await repository.getAll();
    return chatHistory;
}