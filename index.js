import express from "express";
import mongoose from "mongoose";
import ProductRoutes from "./src/routes/productRoutes.js";
import ChatRoutes from "./src/routes/chatRoutes.js";

import { getAllProducts } from "./src/services/productService.js";
import { saveNewMessage } from ".//src/services/chatService.js"

import { Server } from "socket.io";
import http from "http";
import session from "express-session"
import MongoStore from 'connect-mongo';

const app = express();
const port = 8080;
const baseUrl = "/api";


const server = http.createServer(app);
const io = new Server(server);
app.locals.io = io;
const mongoDbUrl = "mongodb+srv://testUser:testPassword@cluster0.soy6d.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"

try {
  mongoose.connect(mongoDbUrl, { useNewUrlParser: true })
} catch (e) {
  console.log(e)
  throw new Error("mongo db cannot be connected");
}
app.use(session({
  secret: "secret",
  resave: false,
  saveUninitialized: false,
  store: new MongoStore(
    {
      mongoUrl: mongoDbUrl,
      mongoOptions: {
        useNewUrlParser: true,
        useUnifiedTopology: true
      }
    }
  )
}))


io.on("connection", async (socket) => {
  console.log("a user connected");

  const products = await getAllProducts();
  io.emit("initialization", { products });

  socket.on("new message", (msg) => {
    io.emit("messages", msg);
    saveNewMessage(msg);

  });

});

app.use(express.json());
app.use(express.static("public"));

app.post(baseUrl + "/login", (req, res) => {

  const { name } = req.body;
  console.log(name)
  if (!name || name == "") {
    res.status(400).send(" name is missing ")
  }

  req.session.name = name;
  res.send()
});

app.get(baseUrl + "/me", (req, res) => {
  res.send({ name: req.session.name })
})

app.post(baseUrl + "/logout", (req, res) => {
  req.session.destroy((err) => {
    if (!err) res.send("Log out ok");
    else res.send({ message: "Log out fail", error: e });
  }
  );
})

app.use(baseUrl, ProductRoutes);
app.use(baseUrl, ChatRoutes);

server.listen(port, async () => {
  console.log(`app listening at http://localhost:${port}`);
});


